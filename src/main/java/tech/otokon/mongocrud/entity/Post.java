/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author otokon
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private String title;
    private String content;    
    private String capital;
    private int reviews;
}
