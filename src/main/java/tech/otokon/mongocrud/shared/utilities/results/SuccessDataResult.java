/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.shared.utilities.results;

/**
 *
 * @author otokon
 */
public class SuccessDataResult<T> extends DataResult<T>{
    
    public SuccessDataResult(T data, String Message) {
        super(data, true, Message);
    }
    public SuccessDataResult(T data) {
        super(data, true);
    }
    public SuccessDataResult(String Message){
        super(Message);
    }
    public SuccessDataResult() {
        super(true);
    }
}
