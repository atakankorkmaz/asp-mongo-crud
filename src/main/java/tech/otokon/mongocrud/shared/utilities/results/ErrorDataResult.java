/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.shared.utilities.results;

/**
 *
 * @author otokon
 */
public class ErrorDataResult<T> extends DataResult<T> {
    
    public ErrorDataResult(T data, String Message) {
        super(data, false, Message);
    }
    public ErrorDataResult(T data) {
        super(data, false);
    }
    public ErrorDataResult(String Message){
        super(Message);
    }
    public ErrorDataResult() {
        super(false);
    }
}
