/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.shared.utilities.results;

/**
 *
 * @author otokon
 */
public class Result{
    
    private boolean Success;
    private String Message;

    public Result(boolean Success) {
        this.Success = Success;
    }

    public Result(boolean Success, String Message) {
        this(Success);
        this.Message = Message;
    }

    public Result(String Message) {
        this.Message = Message;
    }
    
    public boolean isSuccess() {
        return Success;
    }

    public String getMessage() {
        return Message;
    }
}
