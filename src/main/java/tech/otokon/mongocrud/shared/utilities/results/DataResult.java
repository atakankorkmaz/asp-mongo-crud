/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.shared.utilities.results;

/**
 *
 * @author otokon
 */
public class DataResult<T> extends Result {
    
    private T Data;
    
    public DataResult(T Data, boolean Success, String Message) {
        super(Success, Message);
        this.Data = Data;
    }
    public DataResult(T Data, boolean Success) {
        super(Success);
        this.Data = Data;
    }

    public DataResult(String Message) {
        super(Message);
    }
    
    public DataResult( boolean Success) {
        super(Success);
    }
    
    public T getData() {
        return Data;
    }
}
