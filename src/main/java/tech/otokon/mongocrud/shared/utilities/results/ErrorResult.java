/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.shared.utilities.results;

/**
 *
 * @author otokon
 */
public class ErrorResult extends Result{

   public ErrorResult(String Message){
       super(false, Message);
   }
   
    public ErrorResult() {
        super(false);
    }
}
