/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.shared.utilities.results;

/**
 *
 * @author otokon
 */
public class SuccessResult extends Result {
    
    public SuccessResult(String Message) {
        super(true, Message);
    }

    public SuccessResult() {
        super(true);
    }
    
}
