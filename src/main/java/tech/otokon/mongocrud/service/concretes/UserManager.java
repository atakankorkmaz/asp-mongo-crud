/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.service.concretes;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.otokon.mongocrud.entity.User;
import tech.otokon.mongocrud.repository.UserRepository;
import tech.otokon.mongocrud.shared.utilities.results.DataResult;
import tech.otokon.mongocrud.shared.utilities.results.SuccessDataResult;
import tech.otokon.mongocrud.service.abstracts.IUserService;

/**
 *
 * @author otokon
 */
@Service
public class UserManager implements IUserService {
    
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public DataResult<List<User>> getAlll() {
        return new SuccessDataResult<List<User>>(userRepository.findAll());
    }
}
