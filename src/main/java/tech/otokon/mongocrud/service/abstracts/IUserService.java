/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.service.abstracts;

import java.util.List;
import org.springframework.stereotype.Service;
import tech.otokon.mongocrud.entity.User;
import tech.otokon.mongocrud.shared.utilities.results.DataResult;

/**
 *
 * @author otokon
 */
@Service
public interface IUserService {
    List<User> getAll();
    DataResult<List<User>> getAlll();
}
