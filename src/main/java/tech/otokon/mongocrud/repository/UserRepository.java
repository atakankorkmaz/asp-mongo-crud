/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import tech.otokon.mongocrud.entity.User;

/**
 *
 * @author otokon
 */
@Repository
public interface UserRepository extends MongoRepository<User,String> {
    
}
