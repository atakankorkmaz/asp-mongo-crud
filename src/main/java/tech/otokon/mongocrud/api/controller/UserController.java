/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tech.otokon.mongocrud.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.otokon.mongocrud.entity.User;
import tech.otokon.mongocrud.shared.utilities.results.DataResult;
import tech.otokon.mongocrud.shared.utilities.results.SuccessDataResult;
import tech.otokon.mongocrud.service.abstracts.IUserService;

/**
 *
 * @author otokon
 */
@RestController
@RequestMapping("api/users")
public class UserController {
    
    @Autowired
    private IUserService userService;
    
    @GetMapping("/getall")
    public List<User> getAll(){
        return userService.getAll();
    }
    
    @GetMapping("/getalll")
    public DataResult<List<User>> getAlll(){
        return userService.getAlll();
    }
}
